'use strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let DiagnosticTicketSchema = new Schema({
    requestedTime : String,
    filename : String,
    gatewayId : String,
    open : Boolean
});

module.exports = mongoose.model('DiagnosticTicket', DiagnosticTicketSchema);