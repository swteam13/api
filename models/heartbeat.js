'use strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let HeartbeatSchema = new Schema({
    gatewayId : {
        type : String,
        required : 'Enter the GatewayId for the heartbeat'
    },
    timestamp : String,
    diagnostics : {
        filename : String,
        requestedTime : String,
        cpu : String,
        battery : String,
        memory : String,
        os : String
    }
});

module.exports = mongoose.model('Heartbeat', HeartbeatSchema);