'use strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let UserSchema = new Schema({
    username : {
        type : String,
        required : 'No username'
    },
    password : {
        type : String,
        required : 'No password'
    }

});

module.exports = mongoose.model('User', UserSchema);