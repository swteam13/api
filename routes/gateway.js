const express = require('express');
const router = express.Router({});
const sendmail = require('sendmail')();

let Heartbeat = require('../models/heartbeat');
let DiagnosticTicket = require('../models/diagnosticTicket');

// Get all heartbeats
router.get('/heartbeats', function (req, res) {
    Heartbeat.find({}, function (err, heartbeat) {
        if (err) return (err);
        res.status(201).send(heartbeat);
    })
});

router.get('/diagnosticTickets', function (req, res) {
    DiagnosticTicket.find({}, function (err, tickets) {
        if (err) return (err);
        res.status(201).send(tickets);
    })
});

// get all heartbeats with specific gatewayId
router.get('/:gatewayId', function (req, res) {
    Heartbeat.find({"gatewayId" : req.params.gatewayId }, function (err, heartbeat) {
        if (err) return (err);
        res.status(201).send(heartbeat);
    })
});

//post a heartbeat
router.post('/heartbeat', function (req, res) {
    let heartbeat = new Heartbeat();
    if(req.body.gatewayId)
    {
        heartbeat.gatewayId = req.body.gatewayId;
        if(req.body.timestamp)
        {
            heartbeat.timestamp = req.body.timestamp
        }
        if ( req.body.diagnostics)
        {
            heartbeat.diagnostics.requestedTime = req.body.diagnostics.requestedTime;
            heartbeat.diagnostics.filename = req.body.diagnostics.filename;
            heartbeat.diagnostics.cpu = req.body.diagnostics.cpu;
            heartbeat.diagnostics.memory = req.body.diagnostics.memory;
            heartbeat.diagnostics.battery = req.body.diagnostics.battery;
            heartbeat.diagnostics.os = req.body.diagnostics.os;
            sendmail({
                from: 'bcoronad@stedwards.edu',
                to: 'baxtercoronado@yahoo.com',
                subject: 'Diagnostic Received',
                html: 'The diagnostic for '+ heartbeat.diagnostics.filename + 'at the time ' + heartbeat.diagnostics.requestedTime + ' is ready!',

            }, function(err, reply) {
                console.log(err && err.stack);
                console.dir(reply);
            });
        }
        
        heartbeat.save(function (err) {
            if (err) return (err);

        });

        DiagnosticTicket.findOne({"gatewayId" : heartbeat.gatewayId, "open" : true }, function (err, ticket) {
            if (err) return (err);
            if (ticket === null) {
                res.status(201).json({})
            }
            else {
                const requestedTime = ticket.requestedTime;
                const filename = ticket.filename;

                DiagnosticTicket.updateOne({"requestedTime" : requestedTime, "open" : true }, {"open" : false}, function(err, raw) {
                    if (err) {
                        res.send(err);
                    }
                });
                res.status(201).json({"filename" : filename, "requestedTime" : requestedTime})
            }
        });
    }
    else {
        res.status(412).send(req.body);
    }
});

// Request diagnostic information
// Does not return information -> must use the diagnostic get to retrieve information
router.post('/diagnostic', function (req, res) {
    if(req.body.filename && req.body.gatewayId) {

        let diagnosticTicket = new DiagnosticTicket;
        diagnosticTicket.filename = req.body.filename;
        diagnosticTicket.requestedTime = new Date();
        diagnosticTicket.gatewayId = req.body.gatewayId;
        diagnosticTicket.open = true;

        diagnosticTicket.save(function (err) {
            if (err) return (err);
        });

        res.status(201).json({"requestedTime" : diagnosticTicket.requestedTime});

    }
    else {
        res.status(412).send("Did not include the filename for the diagnostic test");
    }
});

/* GET users listing. */
router.post('/diagnosticInformation', function (req, res) {
    Heartbeat.findOne({"diagnostics.requestedTime" : req.body.requestedTime}, function (err, heartbeat) {
        if (err) return (err);
        if (heartbeat)
        {
            res.status(201).json(heartbeat);
        }
        else {
            res.status(206).send("We don't have that yet.. please try again soon");
        }

    });
});


module.exports = router;