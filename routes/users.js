const express = require('express');
const router = express.Router({});
const mongoose = require('mongoose');
const sha256 = require('js-sha256');


let User = require('../models/user');



router.post('/register', function (req, res) {
    let user = new User;
    user.username = req.body.username;
    user.password = sha256(req.body.password);

    User.findOne({"username" : user.username}, function (err, potentialUser) {
        if (err) {
            return res.send(500, { message: err.message });
        }
        if (potentialUser) {
            res.status(401).send("An account with this email has already been created");
        }
        else {
            if (req.body.username && req.body.password) {

                user.save(function (err) {
                    if (err) return (err);

                });
                res.status(201).send("User successfully added");
            }
            else {
                res.status(412).send("email or password not sent")
            }
        }
    });
});

router.post('/login', function (req, res) {
    let user = new User;

    if (req.body.username && req.body.password) {
        user.username = req.body.username;
        user.password = sha256(req.body.password);
        User.findOne({"username" : user.username, "password" : user.password}, function (err, potentialUser) {
            if (err) return (err);
            if (potentialUser) {
                res.status(201).send("found em");
            }
            else {
                res.status(400).send("No user found with those credentials");
            }
        });

    }
    else {
        res.status(412).send("username or password not sent")
    }


});


module.exports = router;
